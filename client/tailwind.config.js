module.exports = {
  content: [
    "./lib/**/*.{js,ts,jsx,tsx,css}",
    "./pages/**/*.{js,ts,jsx,tsx,css}",
    "./styles/**/*.css",
  ],
  plugins: [],
};
