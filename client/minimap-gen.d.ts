
function ParseEELVLData(eelvlData: Uint8Array): Uint8Array;

function ParseEEArchiveData(archivEEData: Uint8Array, opts: {
    width: number,
    height: number
}): Uint8Array;

declare module 'minimap-gen-pkg' {
    export type ParsedEELVLData = ParsedEELVLData;
    export const parse_eelvl_data: typeof ParseEELVLData;
    export const parse_archivee_data: typeof ParseEEArchiveData;
}
