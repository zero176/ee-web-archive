import { useState } from 'react';
import {
  Hydrate,
  QueryClient,
  QueryClientProvider
} from '@tanstack/react-query';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import NextNProgress from 'nextjs-progressbar';
import { Header } from 'lib/header';
import { WASMContextProvider, MinimapWorkerContextProvider } from 'lib/minimap';
import { Wrapper } from 'lib/layout';
import 'styles/index.css';

const App = ({ Component, pageProps }: AppProps) => {
  const [queryClient] = useState(() => new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnReconnect: false
      }
    }
  }));

  return (
    <>
      <Head>
        <title>EE Web Archive</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="0176" />
        <meta name="theme-color" content="#fbbf24" />
        <meta property="og:type" content="object" />
        <meta property="og:site_name" content="EE Web Archive" />
        <meta property="og:image" content="/favicon.png" />
        <meta property="og:image:width" content="16" />
        <meta property="og:image:height" content="16" />
        <meta property="twitter:image" content="/favicon.png" />
        <link rel="prefetch" as="image" href="/bgblock.png" />
        <link rel="prefetch" as="image" href="/fonts/PixelOperator.ttf" />
        <link rel="prefetch" as="image" href="/fonts/PixelOperator-Bold.ttf" />
      </Head>
      <QueryClientProvider client={queryClient}>
        <Hydrate state={pageProps.dehydratedState}>
          <MinimapWorkerContextProvider>
            <WASMContextProvider>
                <Header />
              <Wrapper>
                <Component {...pageProps} />
              </Wrapper>
            </WASMContextProvider>
          </MinimapWorkerContextProvider>
        </Hydrate>
      </QueryClientProvider>
      <NextNProgress height={10} />
    </>
  );
};

export default App;
