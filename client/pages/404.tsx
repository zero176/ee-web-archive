import { NextPage } from 'next';

const NotFound: NextPage = () => {
  return (
    <div className="w-full text-center mt-5">
      Sorry, this thing does not exist.
    </div>
  );
};

export default NotFound;
