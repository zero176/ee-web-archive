import { useState } from 'react';
import Link from 'next/link';
import { useQuery } from '@tanstack/react-query';
import { UploadedWorldList, UploadedWorld } from 'lib/world-upload';
import { clientKyApi } from 'lib/api';
import { GetServerSideProps, NextPage } from 'next';
import { Pagination } from 'lib/pagination';

interface UploadedWorldsPageProps {
  worldQuery: string;
}

interface UploadedWorldListResult {
  count: number;
  list: UploadedWorld[];
}

export const UploadedWorldsPage: NextPage<UploadedWorldsPageProps> = ({
  worldQuery
}) => {
  const [page, setPage] = useState<number>(0);
  const [newQuery, setNewQuery] = useState('');

  const { isLoading, data } = useQuery({
    queryKey: ['world-search', worldQuery, page],
    queryFn: () => clientKyApi
      .get('world-upload/search', {
        searchParams: {
          worldQuery, page
        }
      })
      .json<UploadedWorldListResult>()
  });

  if (isLoading) {
    return <div className="w-full text-center pt-6">Loading...</div>;
  }

  const { count, list } = data!;

  const paginationInfo = worldQuery === ''
    ? `Found ${count} uploaded worlds total`
    : `Found ${count} uploaded worlds matching ${worldQuery}`;

  return (
    <div className="p-5 overflow-y-scroll">
      <div className="flex flex-col md:flex-row items-center justify-center pb-6 gap-10 text-center">
        <div className="flex flex-col md:flex-row items-center gap-5">
          <div className="text-center text-lg font-bold">{paginationInfo}</div>
          <Pagination
            page={page}
            onPageChange={setPage}
            count={count}
            preventChange={isLoading}
          />
          <form
            method="get"
            action={`/world-upload/search/${newQuery}`}
            className='world-upload-search-form'
          >
            <input value={newQuery} onChange={e => setNewQuery(e.target.value)} />
            <button type="submit">Search</button>
          </form>
        </div>
        <Link href="/world-upload/new">
          <a className='text-pink-400 text-xl font-bold underline underline-offset-4'>
            Upload a new world
          </a>
        </Link>
      </div>
      {isLoading
        ? <div className="text-center pt-6">Loading...</div>
        : (
          <UploadedWorldList
            worlds={list!}
          />)}
      {!isLoading && (
        <Pagination
          page={page}
          onPageChange={setPage}
          count={count}
          preventChange={isLoading}
        />
      )}
    </div >
  );
};

export default UploadedWorldsPage;

export const getServerSideProps: GetServerSideProps<
  UploadedWorldsPageProps,
  { worldQueryParams: string[] }
> = async (context) => {
  const worldQueryParams = context.params?.worldQueryParams ?? [''];

  if (worldQueryParams.length > 1) {
    return {
      notFound: true
    };
  }

  const [worldQuery] = worldQueryParams;

  return {
    props: {
      worldQuery
    }
  };
};
