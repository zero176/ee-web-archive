import React from 'react';
import type { GetServerSideProps, NextPage } from 'next';
import { serverKyApi } from 'lib/api';
import type { EECrew } from 'lib/crew';
import { WorldsList } from 'lib/worlds';
import { PlayerTags } from 'lib/player';

interface CrewPageProps {
  crew: EECrew;
}

const CrewPage: NextPage<CrewPageProps> = ({ crew }) => {
  return (
    <div className="overflow-auto">
      <div className="flex flex-col justify-center items-center gap-2">
        <h1 className="text-3xl font-bold">{crew.name}</h1>
        <h3 className="text-xl">Owned by <span className="font-bold">{crew.ownerName}</span></h3>
        <div className="max-w-[700px]">
          <PlayerTags players={crew.members} />
        </div>
      </div>
      <div className="pt-2">
        <WorldsList showOwner worlds={crew.worlds} />
      </div>
    </div>
  );
};

export default CrewPage;

export const getServerSideProps: GetServerSideProps<
  CrewPageProps,
  { crewId: string }
> = async (context) => {
  const playerName = context.params?.crewId as string | undefined;
  if (!playerName) {
    return {
      notFound: true
    };
  }
  const data = await serverKyApi.get(`crew/${playerName}`).json<EECrew>();
  data.members.sort();
  return {
    props: {
      crew: data
    }
  };
};
