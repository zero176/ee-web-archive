import type { NextPage } from 'next';

const Home: NextPage = () => {
  return (
    <div className="w-full text-center text-xl flex flex-col gap-3 pt-8">
      <p>Hello! Search players or worlds above to get started.</p>
    </div>
  );
};

export default Home;
