import React from 'react';
import type { GetServerSideProps, NextPage } from 'next';
import { dehydrate, QueryClient, useQuery } from '@tanstack/react-query';
import { clientKyApi, serverKyApi } from 'lib/api';
import type { EEPlayer } from 'lib/player';
import { EEWorld, WorldsList } from 'lib/worlds';
import { PlayerCard } from 'lib/player';

interface PlayerPageProps {
  playerName: string
}

const PlayerPage: NextPage<PlayerPageProps> = ({ playerName }) => {
  const playerInfoQuery = useQuery({
    queryKey: [`player-info-${playerName}`],
    queryFn: () => clientKyApi.get(`player/${playerName}`).json<EEPlayer>()
  });

  const playerWorldsQuery = useQuery({
    queryKey: [`player-worlds-${playerName}`],
    queryFn: () => clientKyApi.get(`player/worlds/${playerName}`)
      .json<EEWorld[]>()
  });

  if (playerInfoQuery.isError || playerWorldsQuery.isError) {
    return <div className="text-center pt-6">An error has occurred.</div>;
  }

  if (playerInfoQuery.isLoading || playerWorldsQuery.isLoading) {
    return <div className="text-center pt-6">Loading...</div>;
  }

  const player = playerInfoQuery.data;
  player.friends.sort((a, b) => a.name.localeCompare(b.name));

  const worlds = playerWorldsQuery.data;

  return (
    <div
      key={player.name} // reset scroll position on change
      className="flex flex-col lg:flex-row h-full justify-center xl:gap-12"
    >
      <PlayerCard player={player} />
      <div className="w-full overflow-auto">
        <WorldsList worlds={worlds} />
      </div>
    </div>
  );
};

export default PlayerPage;

export const getServerSideProps: GetServerSideProps<
  PlayerPageProps,
  { playerName: string }
> = async (context) => {
  const playerName = context.params?.playerName as string | undefined;
  if (!playerName) {
    return {
      notFound: true
    };
  }

  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    [`player-info-${playerName}`],
    () => serverKyApi.get(`player/${playerName}`).json<EEPlayer>()
  );

  return {
    props: {
      playerName,
      dehydratedState: dehydrate(queryClient)
    }
  };
};
