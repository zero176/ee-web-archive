import type { GetServerSideProps, NextPage } from 'next';
import { dehydrate, QueryClient, useQuery } from '@tanstack/react-query';
import { WorldsList, buildWorldsQueries } from 'lib/worlds';
import {
  clientKyApi,
  serverKyApi
} from 'lib/api';
import { Pagination } from 'lib/pagination';
import { useRouter } from 'next/router';

interface WorldsLookupPageProps {
  worldQuery: string;
  page: number;
}

const WorldsLookupPage: NextPage<WorldsLookupPageProps> = ({
  worldQuery,
  page
}) => {
  const worldQueries = buildWorldsQueries(clientKyApi);

  const getPaginatedWorlds = worldQueries.getPaginatedWorlds(worldQuery, page);
  const getPaginatedWorldsQuery = useQuery({
    queryKey: getPaginatedWorlds.key,
    queryFn: getPaginatedWorlds.query
  });

  const getWorldCount = worldQueries.getWorldCount(worldQuery);
  const getWorldCountQuery = useQuery({
    queryKey: getWorldCount.key,
    queryFn: getWorldCount.query
  });

  const router = useRouter();
  const navigateToPage = (p: number) => {
    router.push(`/worlds/${worldQuery}/${p}`);
  };

  if (getPaginatedWorldsQuery.isLoading || getWorldCountQuery.isLoading) {
    return <div className="text-center pt-6">Loading...</div>;
  }

  const worldCount = getWorldCountQuery.data!;
  if (worldCount === 0) {
    return (
      <div className="w-full text-center mt-8">
        No worlds matching {worldQuery} found.
      </div>
    );
  }

  const worlds = getPaginatedWorldsQuery.data!;
  return (
    <div className="flex flex-col justify-center items-center gap-1 overflow-auto">
      <div className="h-full">
        <div className="w-full">
          <Pagination
            startIndexAt={1}
            page={page}
            onPageChange={navigateToPage}
            count={worldCount}
            preventChange={false}
          />
        </div>
        <div className="w-full px-3">
          <WorldsList
            showOwner
            header={`Found ${worldCount} worlds matching ${worldQuery}`}
            worlds={worlds}
          />
        </div>
      </div>
    </div>
  );
};

export default WorldsLookupPage;

export const getServerSideProps: GetServerSideProps<
  WorldsLookupPageProps,
  { worldQuery: string, page: string }
> = async (context) => {
  const worldQuery = context.params?.worldQuery as string | undefined;
  if (!worldQuery) {
    return {
      notFound: true
    };
  }

  let page = Number(context.params?.page);
  if (Number.isNaN(page) || page <= 0) {
    page = 1;
  }

  const worldQueries = buildWorldsQueries(serverKyApi);

  const getPaginatedWorlds = worldQueries.getPaginatedWorlds(worldQuery, page);
  const getWorldCount = worldQueries.getWorldCount(worldQuery);

  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    getPaginatedWorlds.key,
    getPaginatedWorlds.query
  );
  await queryClient.prefetchQuery(
    getWorldCount.key,
    getWorldCount.query
  );

  return {
    props: {
      worldQuery,
      page,
      dehydratedState: dehydrate(queryClient)
    }
  };
};
