const path = require("path")

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  images: {
    domains: ["api.sirjosh3917.com"],
  },
  webpack(config) {
    config.resolve.alias = {
      ...config.resolve.alias,
      'minimap-gen-pkg': '/app/minimap-gen/pkg',
    }
    config.experiments.asyncWebAssembly = true;
    return config
  },
  output: 'standalone',
  experimental: {
    esmExternals: 'loose',
  }
};

module.exports = nextConfig;
