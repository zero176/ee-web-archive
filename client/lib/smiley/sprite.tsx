
export const Smiley = ({ id, scale = 1 }: { id?: number; scale?: number }) => {
  const isDefaultSmiley = id === 0;
  if (!id && !isDefaultSmiley) {
    return (
      <div className="smiley-sprite empty">?</div>
    );
  }

  return (
    <>
      <div
        className="smiley-sprite"
        style={{
          transform: `scale(${scale})`,
          backgroundPosition: `${-26 * id}px 0px`,
          marginRight: `${2 * scale}px`
        }}
      />
    </>
  );
};
