import { createContext, useEffect, useState } from 'react';
import type { ReactNode } from 'react';

export const MinimapWorkerContext = createContext<Worker>({} as Worker);

export const MinimapWorkerContextProvider = ({
  children
}: {
  children: ReactNode
}) => {
  const [state, setState] = useState<Worker | null>(null);

  useEffect(() => {
    const worker = new Worker(new URL('./worker/worker', import.meta.url), { type: 'module' });
    const startup = (e: MessageEvent<{ action: 'has-loaded' }>) => {
      if (e.data.action === 'has-loaded') {
        setState(worker);
      }
    };
    worker.addEventListener('message', startup);
    return () => {
      worker.removeEventListener('message', startup);
    };
  }, []);

  if (state === null) {
    return null;
  }

  return (
    <MinimapWorkerContext.Provider value={state}>
      {children}
    </MinimapWorkerContext.Provider>
  );
};
