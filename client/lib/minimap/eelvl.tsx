import { ImgHTMLAttributes, memo, useCallback } from 'react';
import { useQuery } from '@tanstack/react-query';
import { clientKyApi } from 'lib/api';
import { UploadedWorld } from 'lib/world-upload';
import { MinimapWorkerMessage } from './worker/model';
import { Minimap } from './minimap';

interface MinimapProps extends ImgHTMLAttributes<HTMLCanvasElement> {
  world: UploadedWorld,
  zoom: number,
  generationPriority?: boolean,
  onClicked?: (w: UploadedWorld) => void
}

export const EELVLMinimap = memo((
  { world, zoom, generationPriority, onClicked, ...props }: MinimapProps
) => {
  const { data } = useQuery({
    queryKey: ['eelvl-minimap', world.id],
    queryFn: () => clientKyApi.get(`world-upload/raw/${world.id}`).arrayBuffer()
  });

  const getWorkerMessage = useCallback((offscreenCanvas: OffscreenCanvas) => {
    const message: MinimapWorkerMessage = {
      action: 'paint-eelvl-minimap',
      world,
      eelvlData: new Uint8Array(data!),
      offscreenCanvas,
      urgent: generationPriority
    };
    return message;
  }, [world, data, generationPriority]);

  return (
    <Minimap
      width={world.width}
      height={world.height}
      zoom={zoom}
      data={data}
      getWorkerMessage={getWorkerMessage}
      onClick={() => onClicked?.(world)}
    />
  );
});

EELVLMinimap.displayName = 'EELVLMinimap';
