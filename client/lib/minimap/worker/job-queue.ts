import { MinimapWorkerMessage } from './model';

export class PaintJobQueue<Job extends MinimapWorkerMessage> {
  private jobQueue: Array<Job> = [];
  private previousExecution = 0;

  private process: (j: Job) => void;
  private tick: number;

  constructor ({ tick, process }: { tick: number, process: (j: Job) => void }) {
    this.tick = tick;
    this.process = process;
    this.startProcessingLoop();
  }

  enqueueJob (r: Job): void {
    if (r.urgent) {
      this.jobQueue.unshift(r);
    } else {
      this.jobQueue.push(r);
    }
  }

  startProcessingLoop () {
    const processPaintJobQueue = (time: number) => {
      const elapsed = time - this.previousExecution;
      if (elapsed > this.tick) {
        this.previousExecution = time;
        const latestJob = this.jobQueue.shift();
        if (latestJob) {
          try {
            this.process(latestJob);
          } catch (e) {
            console.error(e);
          }
        }
      }
      self.requestAnimationFrame(processPaintJobQueue);
    };
    self.requestAnimationFrame(processPaintJobQueue);
  }
}
