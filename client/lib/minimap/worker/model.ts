import { UploadedWorld } from 'lib/world-upload';
import { EEWorld } from 'lib/worlds';

// slightly overengineered type system for 2 fucking messages, just for fun

type PaintJobMessage<A extends string, D> = {
    action: A,
    offscreenCanvas: OffscreenCanvas
    urgent?: boolean
} & D

export type PaintArchivEEMinimapMessage = PaintJobMessage<'paint-archivee-minimap', {
    world: EEWorld;
    archivEEData: Uint8Array;
}>

export type PaintEELVLMinimapMessage = PaintJobMessage<'paint-eelvl-minimap', {
    world: UploadedWorld;
    eelvlData: Uint8Array;
}>

export type MinimapWorkerMessage =
    | PaintArchivEEMinimapMessage
    | PaintEELVLMinimapMessage
