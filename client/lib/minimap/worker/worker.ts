import { parse_archivee_data, parse_eelvl_data } from 'minimap-gen-pkg';
import { PaintJobQueue } from './job-queue';
import { MinimapWorkerMessage, PaintArchivEEMinimapMessage, PaintEELVLMinimapMessage } from './model';

const painMinimapIntoCanvas = (
  offscreenCanvas: OffscreenCanvas,
  minimapData: Uint8Array,
  world: {
    width: number
    height: number
  }
) => {
  const ctx = offscreenCanvas.getContext('2d')!;
  const imageData = new ImageData(
    new Uint8ClampedArray(minimapData), world.width, world.height, {
      colorSpace: 'srgb'
    }
  );
  // @ts-expect-error this actually works in Web Workers for some reason?
  ctx.putImageData(imageData, 0, 0);
  console.debug('Minimap painted', world);
  offscreenCanvas.oncontextlost = () => {
    console.debug('context lost');
  };
  offscreenCanvas.oncontextrestored = () => {
    console.debug('context restored');
  };
};

const archivEEPainter = new PaintJobQueue<PaintArchivEEMinimapMessage>({
  tick: 175,
  process: job => {
    const { world, archivEEData, offscreenCanvas } = job;
    const minimapData = parse_archivee_data(archivEEData, {
      width: world.width,
      height: world.height
    });
    painMinimapIntoCanvas(offscreenCanvas, minimapData, world);
  }
});

const eelvlPainter = new PaintJobQueue<PaintEELVLMinimapMessage>({
  tick: 75,
  process: job => {
    const { world, eelvlData, offscreenCanvas } = job;
    const minimapData = parse_eelvl_data(eelvlData);
    painMinimapIntoCanvas(offscreenCanvas, minimapData, world);
  }
});

self.addEventListener('message', async (e: MessageEvent<MinimapWorkerMessage>) => {
  console.debug('Minimap worker received message', e.data);
  const { action } = e.data;
  if (action === 'paint-archivee-minimap') {
    archivEEPainter.enqueueJob(e.data);
  } else if (action === 'paint-eelvl-minimap') {
    eelvlPainter.enqueueJob(e.data);
  }
});

self.postMessage({
  action: 'has-loaded'
});
