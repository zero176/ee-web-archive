import { ImgHTMLAttributes, memo, useContext, useEffect, useRef } from 'react';
import { useIntersection } from 'react-use';
import { MinimapWorkerContext } from './worker-context';
import { MinimapWorkerMessage } from './worker/model';

type MinimapProps = ImgHTMLAttributes<HTMLCanvasElement> & {
  data?: ArrayBuffer;
  zoom: number;
  width: number;
  height: number;
  getWorkerMessage: (offscreenCanvas: OffscreenCanvas) => MinimapWorkerMessage;
  generationPriority?: boolean;
}

export const Minimap = memo((
  {
    data,
    zoom,
    generationPriority,
    width,
    height,
    getWorkerMessage,
    ...props
  }: MinimapProps
) => {
  const minimapWorker = useContext(MinimapWorkerContext);
  const canvasRef = useRef<HTMLCanvasElement | null>(null);

  const hasPaintBeenRequestedRef = useRef(false);
  const canvasIntersection = useIntersection(canvasRef, {
    root: null,
    threshold: 0.5
  });
  const isWorldVisible = canvasIntersection && canvasIntersection.intersectionRatio > 0.5;

  useEffect(() => {
    const canvas = canvasRef.current!;
    const hasPaintBeenRequested = hasPaintBeenRequestedRef.current;
    if (!isWorldVisible || hasPaintBeenRequested || !canvas || !data) {
      return;
    }
    hasPaintBeenRequestedRef.current = true;

    let offscreenCanvas: OffscreenCanvas;
    try {
      offscreenCanvas = canvas.transferControlToOffscreen();
    } catch {
      // it's likely that this canvas has already been rendered before
      // let's just ignore it
      return;
    }

    const message = getWorkerMessage(offscreenCanvas);
    minimapWorker.postMessage(message, [offscreenCanvas]);
  }, [minimapWorker, data, generationPriority, isWorldVisible, getWorkerMessage]);

  return (
    <canvas
      {...props}
      className={'bg-black origin-top-left'}
      width={width}
      height={height}
      style={{
        transform: zoom > 1 ? `scale(${zoom}, ${zoom})` : undefined
      }}
      ref={canvasRef}
    />
  );
});

Minimap.displayName = 'Minimap';
