import { ReactNode, useMemo } from 'react';

const ratioStep = 0.5;
const getSafeRatio = (width: number, height: number, ratio: number): number => {
  const safeWindowWidth = window.innerWidth - 30;
  const safeWindowHeight = window.innerHeight - 60;
  if (width * ratio > safeWindowWidth || height * ratio > safeWindowHeight) {
    return getSafeRatio(width, height, ratio - ratioStep);
  }
  // prevent loss of precision over multiple 0.1 changes
  return Math.round(ratio / ratioStep) * ratioStep;
};

export const ZoomedMinimap = ({
  world,
  render,
  onClose
}: {
  world: {
    id: number | string;
    width: number;
    height: number;
    name: string;
  };
  render: (zoom: number) => ReactNode
  onClose: () => void;
}) => {
  const safeRatio = useMemo(
    () => getSafeRatio(world.width, world.height, 7),
    [world]
  );

  return (
    <div
      key={world.id}
      className="absolute top-0 left-0 right-0 bottom-0 flex justify-center items-center"
      onClick={onClose}
    >
      <div className="absolute top-0 left-0 right-0 bottom-0 bg-black opacity-80 w-full h-full z-40" />
      <div className="z-50 text-center">
        <div>
          <p className="text-xl font-bold">{world.name}</p>
          <p>{world.width}x{world.height} (click anywhere to close)</p>
        </div>
        <div
          className="m-auto"
          style={{
            width: world.width * safeRatio,
            height: world.height * safeRatio
          }}>
          {render(safeRatio)}
        </div>
      </div>
    </div>
  );
};
