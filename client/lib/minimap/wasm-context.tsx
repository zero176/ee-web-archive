import { useState, createContext, useEffect } from 'react';
import type { ReactNode } from 'react';
import * as minimapGenPkg from 'minimap-gen-pkg';

export type MinimapGenWASM = typeof minimapGenPkg;

const placeholder = {
  parse_eelvl_data: () => [new Uint8Array(0), 0, 0] as minimapGenPkg.ParsedEELVLData,
  parse_archivee_data: () => new Uint8Array(0)
};
export const WASMContext = createContext<typeof minimapGenPkg>(placeholder);

export const WASMContextProvider = ({
  children
}: {
  children: ReactNode
}) => {
  const [state, setState] = useState<typeof minimapGenPkg | null>(null);

  useEffect(() => {
    import('minimap-gen-pkg').then(setState);
  }, []);

  if (state === null) {
    return null;
  }

  return (
    <WASMContext.Provider value={state}>
      {children}
    </WASMContext.Provider>
  );
};
