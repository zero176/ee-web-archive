import { useQuery } from '@tanstack/react-query';
import { ImgHTMLAttributes, memo, useCallback } from 'react';
import type { EEWorld } from 'lib/worlds';
import { clientKyApi } from 'lib/api';
import { MinimapWorkerMessage } from './worker/model';
import { Minimap } from './minimap';

interface MinimapProps extends ImgHTMLAttributes<HTMLCanvasElement> {
  world: EEWorld,
  zoom: number,
  generationPriority?: boolean,
  onClicked?: (w: EEWorld) => void
}

export const ArchivEEMinimap = memo((
  { world, zoom, generationPriority, onClicked, ...props }: MinimapProps
) => {
  const { data } = useQuery({
    queryKey: ['archivee-minimap', world.rowId],
    queryFn: () => clientKyApi.get(`worlds/raw/${world.rowId}`).arrayBuffer()
  });

  const getWorkerMessage = useCallback((offscreenCanvas: OffscreenCanvas) => {
    const message: MinimapWorkerMessage = {
      action: 'paint-archivee-minimap',
      world,
      archivEEData: new Uint8Array(data!),
      offscreenCanvas,
      urgent: generationPriority
    };
    return message;
  }, [world, data, generationPriority]);

  return (
    <Minimap
      width={world.width}
      height={world.height}
      zoom={zoom}
      data={data}
      getWorkerMessage={getWorkerMessage}
      onClick={() => onClicked?.(world)}
    />
  );
});

ArchivEEMinimap.displayName = 'ArchivEEMinimap';
