import { PropsWithChildren } from 'react';

export const Wrapper = ({ children }: PropsWithChildren) => {
  return (
    <div className="xl:mx-[5vw] h-full">
      {children}
    </div>
  );
};
