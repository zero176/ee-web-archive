import { EEWorld } from 'lib/worlds';

export interface EECrewMember {
  name: string;
  smileyId: number;
}

export interface EECrew {
  id: string;
  name: string;
  ownerName: string;
  members: EECrewMember[];
  worlds: EEWorld[];
}
