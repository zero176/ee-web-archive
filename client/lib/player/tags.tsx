import Link from 'next/link';
import { Smiley } from '../smiley';

export const PlayerTags = ({
  smallText,
  players
}: {
  smallText?: boolean;
  players: Array<{
    name: string;
    smileyId: number;
  }>;
}) => {
  return (
    <div
      className={`flex flex-wrap justify-center items-center gap-4${
        smallText ? 'text-xs' : ''
      }`}
    >
      {players.map((f) => (
        <Link key={f.name} href={`/player/${f.name}`}>
          <a className="flex items-center">
            <Smiley id={f.smileyId} />
            {f.name}
          </a>
        </Link>
      ))}
    </div>
  );
};
