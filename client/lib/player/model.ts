export interface EEPlayerCrew {
  id: string;
  name: string;
}

export interface EEPlayerFriend {
  id: string;
  name: string;
  smileyId: number;
}

export interface EEPlayer {
  name: string;
  smileyId: number;
  friends: EEPlayerFriend[];
  createdAt: string;
  lastLoginAt: string;
  crews: EEPlayerCrew[];
}
