import React, { ReactNode } from 'react';
import Link from 'next/link';
import { formatDayDate } from 'lib/utils';
import { Smiley } from 'lib/smiley';
import type { EEPlayer } from './model';

const PlayerSection = ({
  header,
  children
}: {
  header: string;
  children?: ReactNode;
}) => (
  <>
    <span className="shrink-0 font-bold mt-4 inline">{header}</span>
    {children}
  </>
);

const PlayerInfo = ({ children }: { children?: ReactNode }) => (
  <span className="shrink-0">{children}</span>
);

export const PlayerCard = ({ player }: { player: EEPlayer }) => {
  return (
    <div className="flex flex-col items-center pt-10 pl-4 pb-[75px] lg:w-[400px] text-md overflow-auto">
      <div className="text-center">
        <Smiley scale={3} id={player.smileyId} />
        <h1 className="pt-3 text-4xl text-amber-300 font-bold ">
          {player.name}
        </h1>
      </div>
      <PlayerInfo>Joined on {formatDayDate(player.createdAt)}</PlayerInfo>
      <PlayerInfo>Last seen on {formatDayDate(player.lastLoginAt)}</PlayerInfo>
      <PlayerSection header={`${player.crews.length} crews`}>
        {player.crews.length
          ? (<div className="grid grid-cols-2 gap-2">
            {player.crews.map(c => (
              <span className="inline-block whitespace-pre" key={c.id}>
                - <Link href={`/crew/${c.id}`}>{c.name}</Link>
              </span>
            ))}
          </div>)
          : null}
      </PlayerSection>
      <PlayerSection header={`${player.friends.length} friends`}>
        {player.friends.length
          ? (<div className="w-full grid grid-cols-2">
            {player.friends.map(f => (
              <div key={f.name}>
                <Link href={`/player/${f.name}`}>
                  <a className="block flex items-center">
                    <Smiley id={f.smileyId} />
                    <span>{f.name}</span>
                  </a>
                </Link>
              </div>
            ))}
          </div>)
          : null}
      </PlayerSection>
    </div>
  );
};
