import { useEffect, useState } from 'react';

/**
 * Next.js uses completely different "server" and "client" environments
 * So we have to do this shit
 */

const getHostname = () => {
  if (typeof window !== 'undefined') {
    return window.location.hostname;
  }
  return '';
};

export const CLIENTSIDE_API_URL =
  getHostname() === 'localhost' ? 'http://localhost:8080/api/' : '/api/';

export const SERVERSIDE_API_URL = `http://${process.env.API_URL}:8080/api/`;

/**
 * I swear this is the stupidest thing I've ever written, but it's because of Next.js's rendering
 * See https://nextjs.org/docs/migrating/from-create-react-app#safely-accessing-web-apis
 */
export const useClientApiURL = () => {
  const [apiURL, setApiURL] = useState<string>('/');
  useEffect(() => {
    setApiURL(CLIENTSIDE_API_URL);
  }, []);
  return apiURL;
};
