import ky from 'ky-universal';
import { CLIENTSIDE_API_URL, SERVERSIDE_API_URL } from './urls';

export const serverKyApi = ky.extend({
  prefixUrl: SERVERSIDE_API_URL,
  retry: 1
});

export const clientKyApi = ky.extend({
  prefixUrl: CLIENTSIDE_API_URL,
  retry: 1
});
