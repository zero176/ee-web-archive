import { WASMContext } from 'lib/minimap';
import Link from 'next/link';
import { ForwardedRef, forwardRef, InputHTMLAttributes, useContext } from 'react';
import { useForm } from 'react-hook-form';
import { clientKyApi } from 'lib/api';

const Field = forwardRef(({ label, className, ...rest }: {
  label: string
} & InputHTMLAttributes<HTMLInputElement>, ref: ForwardedRef<HTMLInputElement>) => {
  return (
    <label>
      {label}
      <input ref={ref} className={`ml-2 text-black font-bold ${className ?? ''}`} {...rest} />
    </label>
  );
});
Field.displayName = 'Field';

interface UploadWorldFormValues {
  worldName: string
  authorName: string
  file: FileList | null
}

export const UploadWorldForm = () => {
  const wasm = useContext(WASMContext);
  const { register, handleSubmit, reset, formState } = useForm<UploadWorldFormValues>();

  const handleUpload = async ({ worldName, authorName, file }: UploadWorldFormValues) => {
    if (!file || file.length !== 1) {
      return;
    }

    const eelvlFile = file[0];
    const eelvlBytes = new Uint8Array(await eelvlFile.arrayBuffer());
    // check if it's a valid .eelvl
    wasm.parse_eelvl_data(eelvlBytes);

    const formData = new FormData();
    formData.append('authorName', authorName);
    formData.append('worldName', worldName);
    formData.append('file', eelvlFile);
    await clientKyApi.post('world-upload', {
      body: formData
    });
    reset();
  };

  const { isSubmitted, isSubmitSuccessful } = formState;

  return (
    <form className="flex flex-col gap-5 p-10" onSubmit={handleSubmit(handleUpload)}>
      <Link href="/world-upload">
        <button>
          Go back
        </button>
      </Link>
      <Field label="World name" type="text" required maxLength={20} {...register('worldName')} />
      <Field label="Author name" type="text" required maxLength={20} {...register('authorName')} />
      <Field label="Select your world" type="file" accept=".eelvl" required className="text-white" {...register('file')} />
      <div>
        <button type="submit" className="bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-4 rounded">
          Upload
        </button>
      </div>
      {isSubmitSuccessful && <div className='text-green-400'>Done!</div>}
      {isSubmitted && !isSubmitSuccessful && <div className='text-red-400'>That ain&apos;t no .eelvl :(</div>}

    </form>
  );
};
