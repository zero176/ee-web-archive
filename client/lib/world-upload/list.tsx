import { useCallback, useState } from 'react';
import { useClientApiURL } from 'lib/api';
import { formatDayDate, shittyTruncateString } from 'lib/utils';
import { EELVLMinimap, ZoomedMinimap } from 'lib/minimap';
import { UploadedWorld } from './model';

export const UploadedWorldList = ({
  worlds
}: {
  worlds: UploadedWorld[];
}) => {
  const clientApiURL = useClientApiURL();
  const [zoomedWorld, setZoomedWorld] = useState<UploadedWorld | null>(null);

  const zoomInOnWorld = useCallback((world: UploadedWorld) => {
    if (zoomedWorld) {
      return;
    }
    setZoomedWorld(world);
  }, [zoomedWorld]);

  return (
    <>
      <div className="flex flex-wrap justify-center gap-6 gap-y-10 pb-6">
        {worlds.map((world) => (
          <div
            key={world.id}
          >
            <div className="text-2xl font-bold">
              {/* too lazy to truncate in CSS */}
              {shittyTruncateString(world.name, 20)}
            </div>
            <div className="pb-1">
              Contributed by{' '} <span className="font-bold">{world.contributorName}</span>
            </div>
            <EELVLMinimap
              world={world}
              zoom={1}
              onClicked={zoomInOnWorld}
            />
            {world.createdAt && (
              <div className="text-sm">
                {world.width}x{world.height} world contributed on {formatDayDate(world.createdAt)}
              </div>
            )}
            <a
              href={`${clientApiURL}world-upload/download/${world.id}`}
              target="_blank"
              rel="noreferrer"
              className="font-bold text-center text-amber-500"
            >
              Download
            </a>
          </div>
        ))}
      </div>
      {zoomedWorld && (
        <ZoomedMinimap
          world={zoomedWorld}
          onClose={() => setZoomedWorld(null)}
          render={(zoom) => (
            <EELVLMinimap
              world={zoomedWorld}
              zoom={zoom}
              onClicked={zoomInOnWorld}
              generationPriority
            />
          )}
        />
      )}
    </>
  );
};
