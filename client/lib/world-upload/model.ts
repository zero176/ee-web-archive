
export interface UploadedWorld {
    id: string
    name: string
    width: number
    height: number
    createdAt: string
    contributorName: string
}
