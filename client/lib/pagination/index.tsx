import ReactPaginate from 'react-paginate';

interface PaginationProps {
  page: number,
  onPageChange: (p: number) => void,
  count: number,
  preventChange: boolean
  startIndexAt?: number
}

export const Pagination = ({
  page, onPageChange, count, preventChange, startIndexAt = 0
}: PaginationProps) => {
  return (
    <ReactPaginate
      breakLabel="..."
      nextLabel=">"
      containerClassName="world-pagination text-lg"
      forcePage={page - startIndexAt}
      onPageChange={(e) => {
        if (preventChange) {
          return;
        }
        onPageChange(e.selected + startIndexAt);
      }}
      pageRangeDisplayed={2}
      pageCount={count ? Math.ceil(count / 10) : 0}
      previousLabel="<"
    />
  );
};
