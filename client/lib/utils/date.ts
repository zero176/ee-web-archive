import { format } from 'date-fns';

export const formatDayDate = (date?: string | null) =>
  !date ? '[REDACTED]' : format(new Date(date), 'P');
