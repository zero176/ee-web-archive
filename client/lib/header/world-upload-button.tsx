import Link from 'next/link';

export const WorldUploadButton = () => {
  return (
    <Link href="/world-upload/search">
      <span className="relative cursor-pointer text-2xl font-bold text-pink-400 underline underline-offset-4">
        Offline World Upload
        <div className="absolute -top-3 -right-3 text-sm font-bold text-white">
          New!
        </div>
      </span>
    </Link>
  );
};
