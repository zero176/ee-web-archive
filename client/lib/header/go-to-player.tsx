import { FormEvent, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { clientKyApi } from 'lib/api';
import { useDebouncedValue } from 'lib/utils';

const isValidPlayerQuery = (q: string) => q && q.length > 1;

export const GoToPlayer = ({
  defaultValue = ''
}: {
  defaultValue?: string;
}) => {
  const router = useRouter();

  const [playerQuery, setPlayerQuery] = useState(defaultValue);
  useEffect(() => {
    setPlayerQuery(defaultValue); // for page changes
  }, [defaultValue]);
  const debouncedPlayerQuery = useDebouncedValue(playerQuery, 500);
  const [isSearchingPlayer, setIsSearchingPlayer] = useState(false);

  const [playerSuggestions, setPlayerSuggestions] = useState<string[]>([]);
  const hasSelectedSuggestion = playerSuggestions.includes(playerQuery);

  useEffect(() => {
    if (!isValidPlayerQuery(debouncedPlayerQuery)) {
      setPlayerSuggestions([]);
      return;
    }

    let hasQueriedAgain = false;
    setIsSearchingPlayer(true);
    clientKyApi
      .get(`player/by-name/${encodeURIComponent(debouncedPlayerQuery)}`)
      .then((resp) => resp.json() as Promise<string[]>)
      .then((suggs) => {
        if (hasQueriedAgain) {
          return;
        }
        setPlayerSuggestions(suggs);
        setIsSearchingPlayer(false);
      });

    return () => {
      hasQueriedAgain = true;
    };
  }, [debouncedPlayerQuery]);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!isValidPlayerQuery(playerQuery) || !hasSelectedSuggestion) {
      return;
    }
    router.push(`/player/${playerQuery}`);
  };

  return (
    <form className="header-form" onSubmit={handleSubmit} autoComplete="">
      <input
        type="datalist"
        list="datalist-data"
        minLength={2}
        value={playerQuery}
        onChange={(e) => setPlayerQuery(e.target.value)}
        placeholder="Type to search"
      />
      <datalist id="datalist-data">
        {playerSuggestions.map((sugg) => (
          <option value={sugg} key={sugg} />
        ))}
      </datalist>
      <button disabled={!hasSelectedSuggestion} type="submit">
        {isSearchingPlayer ? 'Searching...' : 'Go to player'}
      </button>
    </form>
  );
};
