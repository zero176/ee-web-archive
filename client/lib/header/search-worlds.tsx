import { FormEvent, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

const isValidWorldQuery = (q: string) => q && q.length > 2;

export const SearchWorlds = ({
  defaultValue = ''
}: {
  defaultValue?: string;
}) => {
  const router = useRouter();

  const [worldQuery, setWorldQuery] = useState(defaultValue);
  useEffect(() => {
    setWorldQuery(defaultValue); // for page changes
  }, [defaultValue]);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!isValidWorldQuery(worldQuery)) {
      return;
    }
    router.push(`/worlds/${worldQuery}/1`);
  };

  return (
    <form className="header-form" onSubmit={handleSubmit}>
      <input
        minLength={3}
        value={worldQuery}
        onChange={(e) => setWorldQuery(e.target.value)}
      />
      <button
        type="submit"
      >
        Search worlds
      </button>
    </form>
  );
};
