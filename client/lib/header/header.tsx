import Link from 'next/link';
import { useRouter } from 'next/router';
import { GoToPlayer } from './go-to-player';
import { SearchWorlds } from './search-worlds';
import { WorldUploadButton } from './world-upload-button';

export const Header = () => {
  const { query } = useRouter();
  const { playerName, worldQuery } = query as Record<
    string,
    string | undefined
  >;
  return (
    <div className="p-8 px-2 bg-black">
      <div className="flex gap-10 flex-wrap flex-col lg:flex-row items-center justify-center">
        <Link href="/">
          <a className="text-3xl font-bold">EE Web Archive</a>
        </Link>
        <GoToPlayer defaultValue={playerName} />
        <SearchWorlds defaultValue={worldQuery} />
        <WorldUploadButton />
      </div>
    </div>
  );
};
