export interface EEWorld {
  id: string;
  rowId: number;
  name: string;
  plays: number;
  width: number;
  height: number;
  createdAt: string | null;
  ownerName: string;
  byteData?: Uint8Array;
  data?: string
}

export const generateBufferFromWorldData = (world: EEWorld): EEWorld => {
  world.byteData = Buffer.from(world.data!, 'base64');
  delete world.data;
  return world;
};
