import { KyInstance } from 'ky/distribution/types/ky';
import { EEWorld } from './model';

export const buildWorldsQueries = (api: KyInstance) => ({
  getPaginatedWorlds: (worldQuery: string, page: number) => ({
    key: ['world-search', worldQuery, page],
    query: () => api
      .get('worlds/search', {
        searchParams: {
          worldQuery, page: page - 1
        }
      })
      .json<EEWorld[]>()
  }),
  getWorldCount: (worldQuery: string) => ({
    key: ['world-count', worldQuery],
    query: () => api
      .get(`worlds/count?worldQuery=${worldQuery}`)
      .json<number>()
  })
});
