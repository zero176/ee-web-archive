import Link from 'next/link';
import { useCallback, useState } from 'react';
import { useClientApiURL } from 'lib/api';
import { formatDayDate, shittyTruncateString } from 'lib/utils';
import { ZoomedMinimap, ArchivEEMinimap } from 'lib/minimap';
import type { EEWorld } from './model';

export const WorldsList = ({
  worlds,
  header = `${worlds.length} worlds`,
  showOwner = false
}: {
  worlds: EEWorld[];
  header?: string;
  showOwner?: boolean;
}) => {
  const clientApiURL = useClientApiURL();
  const [zoomedWorld, setZoomedWorld] = useState<EEWorld | null>(null);

  const zoomInOnWorld = useCallback((world: EEWorld) => {
    if (zoomedWorld) {
      return;
    }
    setZoomedWorld(world);
  }, [zoomedWorld]);

  return (
    <>
      <div className="text-center text-lg font-bold p-3">{header}</div>
      <div className="flex flex-wrap justify-around gap-6 gap-y-10 mx-6 pb-[200px]">
        {worlds.map((world) => (
          <div
            className="flex flex-col items-center justify-left"
            key={world.id}
          >
            <div className="text-2xl font-bold">
              {/* too lazy to truncate in CSS */}
              {shittyTruncateString(world.name, 20)}
            </div>
            {showOwner && (
              <div className="pb-1">
                Owned by{' '}
                <Link href={`/player/${world.ownerName}`}>
                  <a className="font-bold">{world.ownerName}</a>
                </Link>
              </div>
            )}
            <ArchivEEMinimap
              world={world}
              zoom={1}
              onClicked={zoomInOnWorld}
            />
            {world.createdAt && (
              <div className="text-sm">
                {world.width}x{world.height}, created on{' '}
                {formatDayDate(world.createdAt)}
              </div>
            )}
            <div className="text-sm font-bold">{world.plays} plays</div>
            <a
              href={`${clientApiURL}worlds/download/${world.id}`}
              target="_blank"
              rel="noreferrer"
              className="font-bold text-center text-amber-500"
            >
              Download
            </a>
            <a
              href={`https://pixelwalker.net/world/legacy:${world.id}`}
              target="_blank"
              rel="noreferrer"
              className="font-bold text-center text-sky-400"
            >
              Play
            </a>
          </div>
        ))}
      </div>
      {zoomedWorld && (
        <ZoomedMinimap
          world={zoomedWorld}
          onClose={() => setZoomedWorld(null)}
          render={(zoom) => (
            <ArchivEEMinimap
              world={zoomedWorld}
              zoom={zoom}
              onClicked={zoomInOnWorld}
              generationPriority
            />
          )}
        />
      )}
    </>
  );
};
