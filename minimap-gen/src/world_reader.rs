use std::io;

use crate::byte_reader::ByteReader;
use crate::ee_info::{self, BlockType, LayeredBlock, WorldMeta};

pub fn read_world_meta(world_stream: &mut &[u8]) -> io::Result<WorldMeta> {
    let owner_name = world_stream.read_utf8_string()?;
    let world_name = world_stream.read_utf8_string()?;
    let width = world_stream.read_be_i32()?;
    let height = world_stream.read_be_i32()?;
    let gravity = world_stream.read_be_f32()?;
    let bg_color = world_stream.read_be_u32()?;
    let description = world_stream.read_utf8_string()?;
    let campaign = world_stream.read_bool()?;
    let crew_id = world_stream.read_utf8_string()?;
    let crew_name = world_stream.read_utf8_string()?;
    let crew_status = world_stream.read_be_i32()?;
    let minimap = world_stream.read_bool()?;
    let owner_id = world_stream.read_utf8_string()?;

    return Ok(WorldMeta {
        owner_name,
        world_name,
        width,
        height,
        bg_color,
        campaign,
        crew_id,
        crew_name,
        crew_status,
        description,
        gravity,
        minimap,
        owner_id,
    });
}

pub fn read_visible_world_blocks(world_data: &mut &[u8]) -> io::Result<Vec<LayeredBlock>> {
    let mut blocks: Vec<LayeredBlock> = Vec::new();
    while world_data.len() > 0 {
        let block = read_layered_block(world_data)?;
        let alpha_value = block.color.3;
        if alpha_value == 255 && block.layer <= 1 {
            blocks.push(block);
        }
    }

    // for iterating through background first when painting
    blocks.sort_by(|a, b| b.layer.cmp(&a.layer));

    return Ok(blocks);
}

fn read_layered_block(world_data: &mut &[u8]) -> io::Result<LayeredBlock> {
    let block_id = world_data.read_be_i32()?;

    let block_type = ee_info::get_block_type(block_id);
    let color = ee_info::BLOCK_COLORS
        .get(&block_id)
        .cloned()
        .unwrap_or(ee_info::DEFAULT_EMPTY_BLOCK_COLOR);

    let layer = world_data.read_be_i32()?;

    let xs = world_data.read_ushort_array()?;
    let ys = world_data.read_ushort_array()?;
    let positions: Vec<(u32, u32)> = xs
        .into_iter()
        .zip(ys.into_iter())
        .map(|(x, y)| (x as u32, y as u32))
        .collect();

    match block_type {
        BlockType::Morphable
        | BlockType::Rotatable
        | BlockType::RotatableButNotReally
        | BlockType::Number
        | BlockType::Enumerable
        | BlockType::Music => {
            world_data.read_be_i32()?;
        }
        BlockType::Portal => {
            world_data.read_be_i32()?;
            world_data.read_be_i32()?;
            world_data.read_be_i32()?;
        }
        BlockType::Sign | BlockType::WorldPortal => {
            world_data.read_utf8_string()?;
            world_data.read_be_i32()?;
        }
        BlockType::Label => {
            world_data.read_utf8_string()?;
            world_data.read_utf8_string()?;
            world_data.read_be_i32()?;
        }
        BlockType::NPC => {
            world_data.read_utf8_string()?;
            world_data.read_utf8_string()?;
            world_data.read_utf8_string()?;
            world_data.read_utf8_string()?;
        }
        _ => (),
    }

    return Ok(LayeredBlock {
        block_id,
        layer,
        color,
        positions,
    });
}
