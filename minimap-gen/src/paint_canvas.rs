use crate::ee_info::{LayeredBlock, RGBAColor};

pub fn get_minimap_data(
    blocks: Vec<LayeredBlock>,
    width: u32,
    height: u32
) -> Vec<u8> {
    let image_data_length = (width * height * 4u32) as usize;
    let image_data_bg_base = vec![0u8, 0u8, 0u8, 255u8];
    // fill the vector with black
    // TODO not very performant, taking up to 100ms sometimes
    let mut image_data_vec: Vec<u8> = image_data_bg_base
        .into_iter().cycle().take(image_data_length).collect::<Vec<u8>>();

    for block in blocks {
        for (x, y) in &block.positions {
            let index: u32 = y * (width * 4) + x * 4;
            let image_data_index = index as usize;
            let RGBAColor(r, g, b, a) = block.color;
            image_data_vec[image_data_index] = r;
            image_data_vec[image_data_index + 1] = g;
            image_data_vec[image_data_index + 2] = b;
            image_data_vec[image_data_index + 3] = a;
        }
    }
    
    return image_data_vec;
}
    