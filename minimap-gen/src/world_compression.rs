use libflate::deflate::Decoder;
use std::io::{self, BufReader, Read};

pub fn decompress_with_deflate(compressed_data: &[u8]) -> io::Result<Vec<u8>> {
    let mut decoder = Decoder::new(compressed_data);
    let mut output = Vec::new();
    decoder.read_to_end(&mut output)?;
    return Ok(output);
}

pub fn decompress_with_lzma(compressed_data: &[u8]) -> lzma_rs::error::Result<Vec<u8>> {
    // we need to preprend the LZMA headers
    // https://github.com/frizb/FirmwareReverseEngineering/blob/master/IdentifyingCompressionAlgorithms.md
    let lzma_headers: &[u8; 13] = &[
        93, 0, 0, 16, 0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    ];
    let lzma_data = [lzma_headers, compressed_data].concat();

    let mut input = BufReader::new(lzma_data.as_slice());
    let mut output: Vec<u8> = Vec::new();
    lzma_rs::lzma_decompress(&mut input, &mut output)?;

    return Ok(output);
}
