use std::io::{Read, self};
use byteorder::{BigEndian, ReadBytesExt};

pub trait ByteReader: Read {
    fn read_be_i32(&mut self) -> io::Result<i32> {
        self.read_i32::<BigEndian>()
    }
    fn read_be_u32(&mut self) -> io::Result<u32> {
        self.read_u32::<BigEndian>()
    }
    fn read_be_u16(&mut self) -> io::Result<u16> {
        self.read_u16::<BigEndian>()
    }
    fn read_be_f32(&mut self) -> io::Result<f32> {
        self.read_f32::<BigEndian>()
    }
    fn read_ushort_array(&mut self) -> io::Result<Vec<u16>> {
        let length = self.read_be_u32()?;
        let mut data = vec![0; length as usize];
        self.read_exact(&mut data)?;
        let ushort_array = data
            .chunks_exact(2)
            .map(|x| u16::from_be_bytes([x[0], x[1]]))
            .collect::<Vec<u16>>();
        return Ok(ushort_array);
    }
    fn read_bool(&mut self) -> io::Result<bool> {
        let byte = self.bytes().next().transpose()?;
        return Ok(byte.map_or(false, |b| b != 0));
    }
    fn read_utf8_string(&mut self) -> io::Result<String> {
        let length = self.read_be_u16()?;

        let mut data = vec![0; length as usize];
        self.read_exact(&mut data)?;

        let err = io::Error::new(io::ErrorKind::InvalidData, "Could not read string");
        return String::from_utf8(data).or(Err(err));
    }
}

impl<R: Read> ByteReader for R {}