use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};
extern crate console_error_panic_hook;

mod ee_info;
mod byte_reader;
mod world_reader;
mod world_compression;
mod paint_canvas;

#[wasm_bindgen(start)]
pub fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
}

#[wasm_bindgen]
pub fn parse_eelvl_data(
    eelvl_data: &[u8]
) -> Vec<u8> {
    let decompressed = world_compression::decompress_with_deflate(eelvl_data)
        .expect("Error when decompressing .eelvl");
    let world_stream = &mut decompressed.as_slice();
    
    let world_meta = world_reader::read_world_meta(world_stream)
        .expect("Error when getting world metadata");

    let blocks = world_reader::read_visible_world_blocks(world_stream)
        .expect("Error when reading visible world blocks");

    return paint_canvas::get_minimap_data(
        blocks,
        world_meta.width as u32,
        world_meta.height as u32,
    );
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ArchivEEMinimapGenOptions {
    width: u32,
    height: u32,
}

#[wasm_bindgen]
pub fn parse_archivee_data(
    archivee_data: &[u8],
    js_opts: JsValue,
) -> Vec<u8> {
    let opts: ArchivEEMinimapGenOptions = serde_wasm_bindgen::from_value(js_opts)
        .expect("Couldn't deserialize options");

    let decompressed_world_data = world_compression::decompress_with_lzma(&archivee_data)
        .expect("Error when decompressing ArchivEE data");
    let world_stream = &mut decompressed_world_data.as_slice();

    let blocks = world_reader::read_visible_world_blocks(world_stream)
        .expect("Error when reading visible world blocks");

    return paint_canvas::get_minimap_data(blocks, opts.width, opts.height);
}