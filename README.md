# ee-web-archive
A web-based hub for Everybody Edits data after its end in 2021.

The "stable" version is available at https://archive.offlinee.com/.
A new version with support for uploaded worlds from OfflinEE is being developed.

## Structure

- **/client** - Next.js UI
- **/minimap-gen** - Rust + WebAssembly parsing and rendering of minimap data
- **/server** - .NET Core backend
- **/worlds** - Regeneration of EE backup DBs
- **/dev-scripts** - Helper scripts for development

## Local development

You'll need Docker and Git LFS installed.

1. Run `git lfs checkout` to get the ArchivEE backup from Git LFS.
2. Create an `.env` file based on `.env.example`.
3. Run one of these commands:     
    - Production: `docker compose up`
    - Development: `./dev-scripts/up.sh`

## Thanks to
SirJosh, LukeM, miou, Yonom, Gosha, Kaslai and a lot more people! (let me know if you're one of them)

