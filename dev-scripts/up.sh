#!/bin/bash
set -e
docker compose -f docker-compose.dev.yml up --build --force-recreate -d $@
docker compose -f docker-compose.dev.yml logs --follow -t