using ArchivEE;
using EEWebArchive.Smiley;
using EEWebArchive.World;
using Microsoft.EntityFrameworkCore;

namespace EEWebArchive.Crew;
public static class CrewApi
{
  public static void MapCrewRoutes(this WebApplication app)
  {
    app.MapGet("/api/crew/{id}", async (
        ArchivEEDB db,
        CachedSmileyFinder cachedSmileyFinder,
        string id
    ) =>
    {
      var crew = await db.Crew
              .Where(c => c.Id == id)
              .Select(c => new EECrew
              {
                Id = c.Id,
                RowId = c.RowId,
                Name = c.Name,
                OwnerName = c.Owner.Name
              })
              .FirstOrDefaultAsync();
      if (crew == null)
      {
        return Results.NotFound();
      }
      var members = await db.Crew
              .Where(c => c.Id == id)
              .SelectMany(c => c.Members.Select(m => new
              {
                m.Id,
                m.Name
              }))
              .ToListAsync();
      var cachedMemberSmileyMap = await cachedSmileyFinder
              .GetCachedPlayerSmileys(members.Select(f => f.Id));
      crew.Members = members
              .Select(f =>
              {
                return new EECrewMember
                {
                  Id = f.Id,
                  Name = f.Name,
                  SmileyId = cachedMemberSmileyMap[f.Id]
                };
              });
      crew.Worlds = await db.World
              .Where(w => w.CrewId == crew.RowId)
              .OrderByDescending(w => w.Plays)
              .Select(w => new EEWorld
              {
                Id = w.Id,
                Name = w.Name,
                Plays = w.Plays,
                Width = w.Width,
                Height = w.Height,
                CreatedAt = w.Created,
                OwnerName = w.Owner.Name
              })
              .ToListAsync();
      return Results.Json(crew);
    });

  }
}