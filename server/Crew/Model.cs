using EEWebArchive.World;

namespace EEWebArchive.Crew;
public class EECrew
{
  public string Id { get; set; }
  public ulong RowId { get; set; }
  public string Name { get; set; }
  public string OwnerName { get; set; }
  public IEnumerable<EEWorld> Worlds { get; set; } = new List<EEWorld>();
  public IEnumerable<EECrewMember> Members { get; set; } = new List<EECrewMember>();
}

public class EECrewMember
{
  public string Id { get; set; }
  public string Name { get; set; }
  public int? SmileyId { get; set; }

}
