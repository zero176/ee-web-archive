
namespace EEWebArchive.World;

public class EEWorld
{
  public string Id { get; set; }
  public ulong RowId { get; set; }
  public string Name { get; set; }
  public uint Plays { get; set; }
  public uint Width { get; set; }
  public uint Height { get; set; }
  public DateTime? CreatedAt { get; set; }
  public string OwnerName { get; set; }
  public byte[] Data { get; set; }
}