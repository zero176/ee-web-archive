using ArchivEE;
using EEWebArchive.HackyArchivEE;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EEWebArchive.World;

public static class WorldApi
{
  public static void MapWorldRoutes(this WebApplication app)
  {
    app.MapGet("/api/worlds/count", async (
        HackyArchivEEDB db,
        [FromQuery] string worldQuery
    ) =>
    {
      if (worldQuery.Length <= 2)
      {
        return Results.BadRequest();
      }
      var lowercaseQuery = worldQuery.ToLower();
      var count = await db.World
              .CountAsync(w => w.Name.ToLower().Contains(lowercaseQuery));
      return Results.Ok(count);
    });

    app.MapGet("/api/worlds/search", async (
        HackyArchivEEDB db,
        [FromQuery] string worldQuery,
        [FromQuery] int page
    ) =>
    {
      if (worldQuery.Length <= 2 || page <= -1)
      {
        return Results.BadRequest();
      }
      var limit = 10;
      var lowercaseQuery = worldQuery.ToLower();
      var worlds = await db.World
              .Include(x => x.Owner)
              .Where(w => w.Name.ToLower().Contains(lowercaseQuery))
              .OrderByDescending(w => w.Plays)
              .Select(w => new EEWorld
              {
                Id = w.Id,
                RowId = w.RowId,
                Name = w.Name,
                Plays = w.Plays,
                Width = w.Width,
                Height = w.Height,
                CreatedAt = w.Created,
                OwnerName = w.Owner.Name,
              })
              .Skip(page * limit)
              .Take(limit)
              .ToListAsync();
      return Results.Json(worlds);
    });

    app.MapGet("/api/worlds/raw/{rowId}", async (
        HackyArchivEEDB db,
        ulong rowId
    ) =>
    {
      var worldData = await db.World.AsNoTracking()
              .Where(x => x.RowId == rowId)
              .Select(x => x.RawData)
              .FirstOrDefaultAsync();
      if (worldData == null)
      {
        return Results.NotFound();
      }
      return Results.File(worldData, fileDownloadName: rowId.ToString());
    });

    app.MapGet("/api/worlds/download/{id}", async (
        ArchivEEDB db,
        string id
    ) =>
    {
      var world = await db.World
              .Where(w => w.Id == id)
              .FirstOrDefaultAsync();
      if (world == null)
      {
        return Results.NotFound();
      }
      var memoryStream = new MemoryStream();
      world.EELVL.Save(memoryStream);
      memoryStream.Seek(0, SeekOrigin.Begin);
      return Results.File(memoryStream, fileDownloadName: $"{world.Name}.eelvl");
    });
  }
}