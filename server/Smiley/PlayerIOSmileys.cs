using PlayerIOClient;

namespace EEWebArchive.Smiley;

public class PlayerIOSmileys
{
  private readonly Client playerIOClient;

  public PlayerIOSmileys(Client playerIOClient)
  {
    this.playerIOClient = playerIOClient;
  }

  public int Get(string playerId)
  {
    return GetSmileyOrDefault(
        playerIOClient
            .BigDB
            .Load("PlayerObjects", playerId)
    );
  }

  public IEnumerable<int> GetMultiple(IEnumerable<string> playerIds)
  {
    return playerIOClient
        .BigDB
        .LoadKeys("PlayerObjects", playerIds.ToArray())
        .Select(dbObj => GetSmileyOrDefault(dbObj))
        .ToList();
  }

  private int GetSmileyOrDefault(DatabaseObject dbObj)
  {
    try
    {
      var smiley = (int?)dbObj["smiley"];
      return smiley ?? 0;
    }
    catch
    {
      // some players just don't have smileys
      return 0;
    }
  }
}