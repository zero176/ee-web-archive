using Microsoft.EntityFrameworkCore;
using PlayerIOClient;
using Serilog;

namespace EEWebArchive.Smiley;

public class CachedSmileyFinder
{
  private readonly PlayerIOSmileys playerIOSmileys;
  private readonly PlayerSmileyCache playerSmileyCache;
  private readonly SmileyDBContext smileyDBContext;

  public CachedSmileyFinder(Client playerIOClient, SmileyDBContext smileyDBContext)
  {
    this.playerIOSmileys = new PlayerIOSmileys(playerIOClient);
    this.playerSmileyCache = new PlayerSmileyCache();
    this.smileyDBContext = smileyDBContext;
  }

  public async Task<int?> GetCachedPlayerSmiley(string playerId)
  {
    var cachedSmiley = playerSmileyCache.Get(playerId);
    if (cachedSmiley != null)
    {
      Log.Information("Got cached smiley {CachedSmiley} for player {Player}", cachedSmiley, playerId);
      return cachedSmiley;
    }
    var smiley = await this.smileyDBContext.PlayerSmileys.AsNoTracking()
      .Where(x => x.PlayerId == playerId)
      .Select(x => x.SmileyId)
      .FirstOrDefaultAsync();
    playerSmileyCache.Save(playerId, smiley);
    Log.Information("Saved smiley {UncachedSmiley} in cache for player {Player}", cachedSmiley, playerId);
    return smiley;
  }

  public async Task<IDictionary<string, int>> GetCachedPlayerSmileys(IEnumerable<string> playerIds)
  {
    var playerSmileys = new Dictionary<string, int>();
    if (!playerIds.Any())
    {
      return playerSmileys;
    }

    var playerCacheMap = playerIds
        .Zip(this.playerSmileyCache.GetMultiple(playerIds))
        .ToDictionary(x => x.First, x => x.Second);
    Log.Information("Got player cache map: {CacheLookup}", playerCacheMap);
    foreach (var (cachedPlayerId, smiley) in playerCacheMap.Where(c => c.Value != null))
    {
      playerSmileys.Add(cachedPlayerId, smiley.Value);
    }

    if (playerSmileys.Keys.Count == playerIds.Count())
    {
      // everyone's cached
      return playerSmileys;
    }

    var uncachedPlayerIds = playerCacheMap
        .Where(c => c.Value == null)
        .Select(c => c.Key);
    var uncachedPlayersSmileys = await this.smileyDBContext.PlayerSmileys.AsNoTracking()
        .Where(x => uncachedPlayerIds.Contains(x.PlayerId))
        .Select(x => x.SmileyId)
        .ToListAsync();
    var fetchedPlayerSmileys = uncachedPlayerIds.Zip(uncachedPlayersSmileys);
    Log.Information("Got smileys for uncached players: {UncachedPlayersLookup}", fetchedPlayerSmileys);
    foreach (var (uncachedPlayerId, smiley) in fetchedPlayerSmileys)
    {
      this.playerSmileyCache.Save(uncachedPlayerId, smiley);
      playerSmileys.Add(uncachedPlayerId, smiley);
    }

    // will probably have concurrency issues
    this.playerSmileyCache.UpdateStartupFile();

    return playerSmileys;
  }
}