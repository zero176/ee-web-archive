using System.Text.Json;

namespace EEWebArchive.Smiley;

public class PlayerSmileyCache
{
  private readonly Dictionary<string, int> smileyCache;
  private string CACHE_JSON_PATH = "/smiley-cache/smiley-cache.json";

  public PlayerSmileyCache()
  {
    if (!File.Exists(CACHE_JSON_PATH))
    {
      Directory.CreateDirectory("/smiley-cache");
      File.WriteAllText(CACHE_JSON_PATH, "{}");
    }
    var smileyCacheText = File.ReadAllText(CACHE_JSON_PATH);
    this.smileyCache = JsonSerializer.Deserialize<Dictionary<string, int>>(smileyCacheText);
  }

  public int? Get(string playerId)
  {
    return this.smileyCache.TryGetValue(playerId, out var smiley)
        ? smiley
        : null;
  }

  public void Save(string playerId, int smiley)
  {
    this.smileyCache[playerId] = smiley;
  }

  public IEnumerable<int?> GetMultiple(IEnumerable<string> playerIds)
  {
    return playerIds.Select(pid => this.Get(pid));
  }

  public void UpdateStartupFile()
  {
    var smileyCacheJSON = JsonSerializer.Serialize(this.smileyCache);
    File.WriteAllText(CACHE_JSON_PATH, smileyCacheJSON);
  }
}