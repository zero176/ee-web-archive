
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EEWebArchive.Smiley;

[Table("smileys")]
public class PlayerSmiley
{

  [Column("player_id")]
  [Key]
  public string PlayerId { get; set; }

  [Column("smiley_id")]
  public int SmileyId { get; set; }
}