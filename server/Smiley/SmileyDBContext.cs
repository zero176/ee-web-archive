using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace EEWebArchive.Smiley;

public class SmileyDBContext : DbContext
{
  public DbSet<PlayerSmiley> PlayerSmileys { get; set; }

  protected override void OnConfiguring(DbContextOptionsBuilder options)
  {
    options
        .UseSqlite(new SqliteConnectionStringBuilder
        {
          DataSource = "/smileys/smileys.sqlite3",
          Mode = SqliteOpenMode.ReadOnly
        }.ToString());
#if DEBUG
    options.LogTo(Console.WriteLine);
#endif
  }
}
