using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ArchivEE;

namespace EEWebArchive.HackyArchivEE;

[Table("world")]
public record HackyWorld
{
  [Column("rowid")]
  [Key]
  public ulong RowId { get; private set; }

  [Column("id")]
  public string Id { get; private set; }

  [Column("data")]
  public byte[] RawData { get; private set; }

  [Column("description")]
  public string Description { get; private set; }

  [Column("width")]
  public uint Width { get; private set; }

  [Column("height")]
  public uint Height { get; private set; }

  // TODO add
  // [Column("background_color")]
  // public Color? BackgroundColor { get; private set; }

  [Column("border_type")]
  public uint BorderType { get; private set; }

  [Column("gravity")]
  public float Gravity { get; private set; }

  [Column("minimap")]
  public bool Minimap { get; private set; }

  [Column("crew")]
  public ulong? CrewId { get; private set; }

  [Column("owner")]
  public ulong OwnerId { get; private set; }
  public virtual HackyPlayer Owner { get; private set; }

  [Column("name")]
  public string Name { get; private set; }

  [Column("plays")]
  public uint Plays { get; private set; }

  [Column("created")]
  public DateTime? Created { get; private set; }

  [Column("crew_status")]
  public CrewWorldStatus? CrewStatus { get; private set; }
}