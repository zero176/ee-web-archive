using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EEWebArchive.HackyArchivEE;

// TODO add more attributes to use this in more places
[Table("player")]
public record HackyPlayer
{
  [Column("rowid")]
  [Key]
  public ulong RowId { get; private set; }

  [Column("id")]
  public string Id { get; private set; }

  [Column("name")]
  public string Name { get; private set; }
}