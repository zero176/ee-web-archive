using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace EEWebArchive.HackyArchivEE;

// Hack around ArchivEE's issues with weird queries, especially WorldData
// TOOD maybe just use normal SQL queries? lol nah I think perf is fine as of rn
public class HackyArchivEEDB : DbContext
{
  public DbSet<HackyWorld> World { get; set; }
  public DbSet<HackyPlayer> Player { get; set; }

  protected override void OnConfiguring(DbContextOptionsBuilder options)
  {
    options
        .UseSqlite(new SqliteConnectionStringBuilder
        {
          DataSource = "/archivee/ArchivEE.sqlite3",
          Mode = SqliteOpenMode.ReadOnly
        }.ToString());
#if DEBUG
    options.LogTo(Console.WriteLine);
#endif
  }
}
