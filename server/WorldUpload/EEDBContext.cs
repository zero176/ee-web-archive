using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace EEWebArchive.WorldUpload;

public class WorldUploadDBContext : DbContext
{
  public WorldUploadDBContext(DbContextOptions<WorldUploadDBContext> options) : base(options)
  { }

  public DbSet<UploadedWorld> UploadedWorlds { get; set; }

  protected override void OnConfiguring(DbContextOptionsBuilder options)
  {
    options
        .UseSqlite(new SqliteConnectionStringBuilder
        {
          DataSource = "/eeoffline/db.sqlite3",
          Mode = SqliteOpenMode.ReadWriteCreate
        }.ToString());
#if DEBUG
    options.LogTo(Console.WriteLine);
#endif
  }
}