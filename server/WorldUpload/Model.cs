using System.ComponentModel.DataAnnotations;

namespace EEWebArchive.WorldUpload;

public enum UploadedWorldType
{
  EELVL = 0,
}

public class UploadedWorld
{
  [Key]
  public Guid Id { get; set; }
  public UploadedWorldType Type { get; set; }
  public string Name { get; set; }
  public string ContributorName { get; set; }
  public int Width { get; set; }
  public int Height { get; set; }
  public DateTime? CreatedAt { get; set; }

  public string GetFilePath() => GetFilePath(this.Id);
  public static string GetFilePath(Guid id) => $"/eeoffline/uploads/{id}";

  private static char[] InvalidChars = System.IO.Path.GetInvalidFileNameChars();
  public string GetPrettyFileName() => string.Join(
      "_",
      $"{this.Name}.eelvl".Split(UploadedWorld.InvalidChars, StringSplitOptions.RemoveEmptyEntries)
  );
}