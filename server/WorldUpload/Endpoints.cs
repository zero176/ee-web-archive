using ArchivEE;
using EELVL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EEWebArchive.WorldUpload;

public static class UploadApi
{
  public static void MapEEOfflineRoutes(this WebApplication app)
  {
    app.MapPost("/api/world-upload", async (
        ArchivEEDB db,
        WorldUploadDBContext eeDBContext,
        HttpRequest req
    ) =>
    {
      if (req.Form.Files.Count != 1)
      {
        return Results.BadRequest();
      }

      string worldName = req.Form["worldName"];
      string authorName = req.Form["authorName"];
      if (string.IsNullOrWhiteSpace(worldName) || string.IsNullOrWhiteSpace(authorName))
      {
        return Results.BadRequest();
      }
      var worldFile = req.Form.Files[0];

      // checking if it's a valid .eelvl
      using var worldFileStream = worldFile.OpenReadStream();
      var eeLVL = Level.Open(worldFileStream);

      var id = Guid.NewGuid();
      var uploadedWorld = new UploadedWorld
      {

        Id = id,
        Name = worldName,
        Type = UploadedWorldType.EELVL,
        ContributorName = authorName,
        CreatedAt = DateTime.UtcNow,
        Height = eeLVL.Height,
        Width = eeLVL.Width,
      };
      eeDBContext.UploadedWorlds.Add(uploadedWorld);

      using var dataFileStream = File.Create(UploadedWorld.GetFilePath(id));
      await worldFile.CopyToAsync(dataFileStream);

      await eeDBContext.SaveChangesAsync();

      return Results.Ok();
    });

    app.MapGet("/api/world-upload/search", async (
        WorldUploadDBContext eeDBContext,
        [FromQuery] string worldQuery,
        [FromQuery] int page
    ) =>
    {
      if (page <= -1)
      {
        return Results.BadRequest();
      }
      var lowercaseQuery = worldQuery.ToLower();

      var count = await eeDBContext.UploadedWorlds.AsNoTracking()
        .CountAsync(w => w.Name.ToLower().Contains(lowercaseQuery));

      var limit = 10;
      var list = await eeDBContext.UploadedWorlds.AsNoTracking()
        .Where(w => w.Name.ToLower().Contains(lowercaseQuery))
        .OrderByDescending(w => w.CreatedAt)
        .Skip(page * limit)
        .Take(limit)
        .ToListAsync();
      return Results.Json(new
      {
        list,
        count,
      });
    });

    app.MapGet("/api/world-upload/raw/{id}", (
        WorldUploadDBContext eeDBContext,
        Guid id
    ) =>
    {
      return Results.File(UploadedWorld.GetFilePath(id));
    });

    app.MapGet("/api/world-upload/download/{id}", async (
        WorldUploadDBContext eeDBContext,
        Guid id
    ) =>
    {
      var world = await eeDBContext.UploadedWorlds
              .Select(x => new UploadedWorld
              {
                Name = x.Name,
                Id = x.Id,
              })
              .FirstOrDefaultAsync(x => x.Id == id);
      if (world == null)
      {
        return Results.NotFound();
      }
      return Results.File(UploadedWorld.GetFilePath(id), fileDownloadName: world.GetPrettyFileName());
    });
  }
}