using ArchivEE;
using EEWebArchive.Crew;
using EEWebArchive.HackyArchivEE;
using EEWebArchive.Player;
using EEWebArchive.Smiley;
using EEWebArchive.World;
using EEWebArchive.WorldUpload;
using PlayerIOClient;
using Serilog;

var log = new LoggerConfiguration()
  .WriteTo.Console()
  .CreateLogger();
Log.Logger = log;

var playerIOClient = PlayerIO.QuickConnect.SimpleConnect("everybody-edits-su9rn58o40itdbnw69plyw", "zx@zx.zx", "zx", null);
Directory.CreateDirectory("/eeoffline/uploads"); // Docker hates empty directories

var builder = WebApplication.CreateBuilder(args);
#if DEBUG
var isDevelopment = true;
#else
var isDevelopment = false;
#endif

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(options =>
{
  options.AddDefaultPolicy(policy => policy.SetIsOriginAllowed(_ => true));
});
builder.Services.AddScoped<Client>(_ => playerIOClient);
builder.Services.AddScoped<ArchivEEDB>(_ => new ArchivEEDB("/archivee/ArchivEE.sqlite3", isDevelopment));
builder.Services.AddDbContext<HackyArchivEEDB>();
builder.Services.AddDbContext<WorldUploadDBContext>();
builder.Services.AddDbContext<SmileyDBContext>();
builder.Services.AddScoped<CachedSmileyFinder>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
  var services = scope.ServiceProvider;
  var context = services.GetRequiredService<WorldUploadDBContext>();
  context.Database.EnsureCreated();
}

if (isDevelopment)
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseCors();

app.MapPlayerRoutes();
app.MapCrewRoutes();
app.MapWorldRoutes();
app.MapEEOfflineRoutes();

app.Run();
