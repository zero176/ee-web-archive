using ArchivEE;
using EEWebArchive.World;
using EEWebArchive.Smiley;
using Microsoft.EntityFrameworkCore;
using EEWebArchive.HackyArchivEE;

namespace EEWebArchive.Player;

public static class PlayerApi
{
  public static void MapPlayerRoutes(this WebApplication app)
  {
    app.MapGet("/api/player/{name}", async (
        ArchivEEDB db,
        CachedSmileyFinder cachedSmileyFinder,
        string name
    ) =>
    {
      if (name.Length <= 2)
      {
        return Results.BadRequest();
      }

      var lowercaseName = name.ToLower();
      var player = await db.Player.AsNoTracking()
              .Where(p => p.Name == lowercaseName)
              .Select(p => new EEPlayer
              {
                Id = p.Id,
                Name = lowercaseName,
                RowId = p.RowId,
                LastLoginAt = p.LastLogin,
                CreatedAt = p.Created,
              })
              .FirstOrDefaultAsync();
      if (player == null)
      {
        return Results.NotFound();
      }

      player.SmileyId = await cachedSmileyFinder
              .GetCachedPlayerSmiley(player.Id);

      var crews = await db.Player.AsNoTracking()
              .Where(p => p.RowId == player.RowId)
              .SelectMany(p => p.Crews.Select(c => new EEPlayerCrew
              {
                Id = c.Id,
                Name = c.Name
              }))
              .ToListAsync();
      player.Crews = crews;

      var friends = await db.Player.AsNoTracking()
              .Where(p => p.RowId == player.RowId)
              .SelectMany(p => p.Friends.Select(f => new
              {
                f.Id,
                f.Name
              }))
              .ToListAsync();
      var cachedFriendSmileyMap = await cachedSmileyFinder
              .GetCachedPlayerSmileys(friends.Select(f => f.Id));
      player.Friends = friends
              .Select(f =>
              {
                return new EEPlayerFriend
                {
                  Id = f.Id,
                  Name = f.Name,
                  SmileyId = cachedFriendSmileyMap[f.Id]
                };
              });
      return Results.Json(player);
    });

    app.MapGet("/api/player/worlds/{name}", async (
        HackyArchivEEDB db,
        CachedSmileyFinder cachedSmileyFinder,
        string name
    ) =>
    {
      if (name.Length <= 2)
      {
        return Results.BadRequest();
      }
      var lowercaseName = name.ToLower();
      var playerId = await db.Player.AsNoTracking()
              .Where(p => p.Name == lowercaseName)
              .Select(p => p.RowId)
              .FirstOrDefaultAsync();
      if (playerId == 0)
      {
        return Results.NotFound();
      }
      var worlds = await db.World.AsNoTracking()
              .Where(w => w.OwnerId == playerId)
              .OrderByDescending(w => w.Plays)
              .Select(w => new EEWorld
              {
                Id = w.Id,
                RowId = w.RowId,
                Name = w.Name,
                Plays = w.Plays,
                Width = w.Width,
                Height = w.Height,
                CreatedAt = w.Created,
                OwnerName = name,
              })
              .ToListAsync();
      return Results.Json(worlds);
    });

    app.MapGet("/api/player/by-name/{name}", async (
        HackyArchivEEDB db,
        string name
    ) =>
    {
      if (name.Length <= 2)
      {
        return Results.BadRequest();
      }
      var lowercaseName = name.ToLower();
      var players = await db.Player.AsNoTracking()
              .Where(x => x.Name.Contains(lowercaseName))
              .Select(player => player.Name)
              .ToListAsync();
      return Results.Json(players);
    });
  }
}