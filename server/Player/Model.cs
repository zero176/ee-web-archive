
namespace EEWebArchive.Player;

public class EEPlayer
{
  public string Id { get; set; }
  public string Name { get; set; }
  public ulong RowId { get; set; }
  public DateTime? LastLoginAt { get; set; }
  public DateTime? CreatedAt { get; set; }
  public int? SmileyId { get; set; }
  public IEnumerable<EEPlayerCrew> Crews { get; set; } = new List<EEPlayerCrew>();
  public IEnumerable<EEPlayerFriend> Friends { get; set; } = new List<EEPlayerFriend>();
}

public class EEPlayerCrew
{
  public string Id { get; set; }
  public string Name { get; set; }
}

public class EEPlayerFriend
{
  public string Id { get; set; }
  public string Name { get; set; }
  public int? SmileyId { get; set; }
}
